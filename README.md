# Live Demo:

https://greenfox.gitlab.io/depth-shadows/

# Idea

I'm trying to come up with a way to generate dynamic real-time shadows in a 2d
world.

I believe the only thing you need is sprite and a hightmap of that sprite.  
However, as an import-time optimization, it might make sense to render a Signed
Distance Field in replace of the hightmap. Later we will need to know if a given
point is inside a space and SDF is perfectly suited for this task.

## Diagram

### Phase 1: ZDepthPass

At first pass, the renderer will just render a grayscale image of a hight map.

This will be called the ZDepthPass

```
Side-on representation of world (1d row of 2d world)

             XXXXXXX
             XXXXXXX
             XXXXXXX
             XXXXXXX <----- Building X
             XXXXXXX
           XXXXXXXXXXX
           XXXXXXXXXXX
           XXXXXXXXXXX
Ground->__________________________
texture:00033888888833000000
```

### Phase 1.5: SDFDepthPass

Optional?

As an optimization, it might make sense to render a Signed Distance Field of the
surface instead. I'm not sure how to do this, but I believe it should be
possible to convert object heightmaps as SDF textures than render those in a
"pick max" (for example, when drawing a large building near a smaller building,
the larger building's hight could overpower the other building)

Thinking this through, I'm not exactly sure how this helps. I think you have to  
render just the heightmaps as well as the SDFDepthPass. Use the SDFDepthPass to
decide how large of a sample jump you can take and use the actual heightmap to
determine if you've blocked the light source.

### Pass 2: ShadowPass

```
           \ <--- sun beam
            \
             \
              \
               \
                \
                 \
                  \
                   \5
             XXXXXXX\
             XXXXXXX \
             XXXXXXX  \
             XXXXXXX   \
             XXXXXXX4   \
           XXXXXXXXXX3\  \
           XXXXXXXXXXX \  \
           XXXXXXXXXXX  2  \  1
```

When trying to decide if a point is inside or outside the shadow, we follow the
angle of the sun beam, sampling the screen-texture of the ZDepthPass
[^Stepping], while incrementing up. Using the Cosine of the angle of the sun, we
can see at what height a sun beam *would* have at every sample point. If at any
point, we sample a pixel that is lower than the heightmap, we know that the sun
is blocked and we can color this pixel as "in shadow".

For point `1`, this will never happen. The pixel will easily reach the light
source, passing above point `5`. For point `2`, this will happen pretty quickly near point '3'.
For point `4`, we see the "shelf shadowing" ability demonstrated.

[^Stepping]: For a first prototype, we will step through sampling ZDepthPass at
a constant rate until we hit a distance threshold. IE: just step 1 pixel over
until we hit over 500 pixels, no building more than 500 pixels away will be able
to cast a shadow. However, this is very inefficient and SDF can fix this.
SDFDepthPass will let us skip sample. If we're rendering an open grassy field,
we can take larger steps than when we're near to a building. SDF gives us a
"maximum possible skip". (once I have this more figured out, I'll diagram it)
I'm not sure, but this SDF style stepping may be dependant on sun angle, which
would make the assumption that SDF is useful here completely wrong.

### Phase 3: ColorPass

I think this part should be easy. Just a simple color pass of the whole world.
Draw all your colored sprites.

### Phase 4: Combine

Combine your ColorPass with the shadow pass.



## Implementation

- Uniforms
  - vec4 LitColor
  - vec4 ShadowColor
  - LightVector (normalized)//idk about this, maybe angle is better?
  - MaxHeight
  - 



## Notes

Figure out SDF.

Normal maps?

Prototype images?


The safe shadow detection distance is based on the height of the highest thing
and the Sine of the sun angle. Knowing that, the SDF might need the sun angle too.


## References

I wrote most of this out before discovering this: https://www.tobias-franke.eu/download/articles/shadowmap/

This paper is from 2003 that talks about using a terrain heightmap to generate a
static shadow map. When this was written, doing this in realtime probably wasn't
possible. SDFs weren't super common either.