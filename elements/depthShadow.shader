shader_type canvas_item;

uniform float direction;
uniform float angle;
uniform bool textureMixing;

uniform vec4 lit_color : hint_color;
uniform vec4 shadow_color : hint_color;





uniform vec2 textureResolution;

const int sampleRate = 256;

varying vec2 sampleVector;// = vec2(1.0,0);
varying vec2 sampleResolution;
varying float sampleSlope;

void vertex(){
	sampleSlope=0.01;
//	VERTEX = VERTEX * (VERTEX.x * VERTEX.y);
	float d = direction * (3.1415*2.0);
	sampleVector = vec2(sin(d),cos(d))/0.25;


	sampleResolution = sampleVector/textureResolution;
}

void fragment(){
	vec4 defaultColor;
	if(textureMixing){
		defaultColor = texture(TEXTURE, UV);
	}
	else{
		defaultColor = vec4(1,1,1,1);//;
	}
	
	
	COLOR = lit_color * defaultColor;
	float startingH = texture(TEXTURE, UV).r;
	
	
	for(int i = 0; i < sampleRate; i++){
		vec2 sampleUV = UV + (sampleResolution*(float(i)));
		float sampleH = texture(TEXTURE,sampleUV).r - (sampleSlope*float(i));
		if(sampleH > startingH){
			COLOR = shadow_color*defaultColor;
			break;
		}
	}
//COLOR = texture(TEXTURE, UV*size);
//	vec2 scale = UV*size;
//	COLOR = vec4(scale,0.0, 1.0);
//	COLOR = vec4(UV.x, 0.0,UV.y, 1.0);
}
